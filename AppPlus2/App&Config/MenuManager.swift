//
//  MenuManager.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 04/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import Foundation
import UIKit

class MenuManager {
    static var shared: MenuManager = MenuManager()
    
    var selectedIndex: Int = 0 {
        didSet{
            DispatchQueue.main.async {
                MenuView.shared.tableView.reloadData()
                MenuView.shared.close()
            }
        }
    }
    
    var rows: [MenuCell] = {
        return Constants.MENU_CELL
    }()
    
}

struct MenuCell {
    var labelTitle: String
    var image: UIImage
    var destiny: UIViewController
}
