//
//  AppDelegate.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 03/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import UIKit
import CoreData
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Firebase
        let filePath = Constants.GOOGLESERVICE_PATH ?? ""

        let options = FirebaseOptions.init(contentsOfFile: filePath)
        if let opt = options {
            print(filePath)
            FirebaseApp.configure(options: opt)
        }

        
        
        guard #available(iOS 13.0, *) else {
            self.window = self.window ?? UIWindow()
            self.window?.rootViewController = UINavigationController(rootViewController:  MainViewController())
            self.window?.makeKeyAndVisible()
            return true
        }
        return true
    }
    

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        
    }
    
    //CoreData
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}




