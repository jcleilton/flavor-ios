//
//  UserViewModel.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 06/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import Foundation

protocol UserViewModalDelegate: class {
    func didFinishedFetching(users: [User])
}

class UserViewModel {
    var users: [User] = []
    weak var delegate: UserViewModalDelegate?
    
    func fetchData() {
        self.users = User.listUsers()
        self.delegate?.didFinishedFetching(users: self.users)
    }
}
