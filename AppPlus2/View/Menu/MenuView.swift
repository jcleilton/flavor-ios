//
//  MenuView.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 04/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import Foundation
import UIKit


class MenuView: UIView {
    
    var menuIsOpened = false
    var menuWidthConstraint: NSLayoutConstraint?
        
    static let shared: MenuView = {
        let menu = MenuView()
        return menu
    }()
    
    lazy var tableView: UITableView = {
        let tbv = UITableView()
        tbv.translatesAutoresizingMaskIntoConstraints = false
        tbv.separatorStyle = .none
        tbv.backgroundColor = .clear
        return tbv
    }()
    
    lazy var titleLabel: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.textAlignment = .center
        lb.textColor = Constants.MAIN_COLOR
        lb.text = Constants.APP_NAME
        return lb
    }()
    
    lazy var imageView: UIImageView = {
        let obj = UIImageView()
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.image = Constants.LOGO
        obj.contentMode = .scaleAspectFill
        return obj
    }()
    
    lazy var button: UIButton = {
        let obj = UIButton()
        obj.setTitle(Constants.MENU_BTN_TXT, for: .normal)
        obj.setImage(Constants.MENU_BTN_IMG, for: .normal)
        obj.setTitleColor(Constants.MAIN_COLOR, for: .normal)
        obj.tintColor = Constants.MAIN_COLOR
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    func layoutMenuInSuperView(view: UIView?) {
        MenuView.shared.setupMenu()
        MenuView.shared.setupTableView()
        self.menuWidthConstraint = self.widthAnchor.constraint(equalToConstant: 0)
        self.menuWidthConstraint?.isActive = true
        if let view = view {
            self.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
            self.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            self.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        
    }
    
    func handleMenu() {
        if !self.menuIsOpened {
            DispatchQueue.main.async { [weak self] in
                if let me = self {
                    me.menuWidthConstraint?.isActive = false
                    me.menuWidthConstraint = me.widthAnchor.constraint(equalToConstant: 200)
                    me.menuWidthConstraint?.isActive = true
                    me.imageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
                    UIView.animate(withDuration: 0.3, animations: {
                        me.superview?.layoutIfNeeded()
                    })
                }
            }
            self.menuIsOpened = !self.menuIsOpened
        } else {
            self.close()
        }
        

    }
    
    func close() {
        if self.menuIsOpened {
            DispatchQueue.main.async {  [weak self] in
                if let me = self {
                    me.menuWidthConstraint?.isActive = false
                    me.menuWidthConstraint = me.widthAnchor.constraint(equalToConstant: 0)
                    me.menuWidthConstraint?.isActive = true
                    guard #available(iOS 13, *) else {
                        me.imageView.heightAnchor.constraint(equalToConstant: 0).isActive = true
                        UIView.animate(withDuration: 0.3, animations: {
                            me.superview?.layoutIfNeeded()
                        })
                        return
                    }
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        me.superview?.layoutIfNeeded()
                    })
                }
                
            }
            
            self.menuIsOpened = false
        }
    }
    
    func setupMenu() {
        
        
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 0.5
        

        
        
        self.addSubview(self.imageView)
        self.addSubview(self.tableView)

        
        self.tableView.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 50).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 40))
        footerView.backgroundColor = .clear
        footerView.addSubview(self.button)
        self.button.centerXAnchor.constraint(equalTo: self.tableView.tableFooterView?.centerXAnchor ?? footerView.centerXAnchor).isActive = true
        self.button.centerYAnchor.constraint(equalTo: self.tableView.tableFooterView?.centerYAnchor ?? footerView.centerYAnchor).isActive = true
        self.button.widthAnchor.constraint(equalToConstant: 90).isActive = true
        self.button.heightAnchor.constraint(equalToConstant: 28).isActive = true
        self.tableView.tableFooterView = footerView
        
        DispatchQueue.main.async {
            self.setupImageConstrants()
        }
        
        
    }
    
    func setupTableView() {
        tableView.register(MenuTableViewCell.self, forCellReuseIdentifier: "menuCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func setupImageConstrants() {
        self.imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        self.imageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -30).isActive = true
        self.imageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30).isActive = true
        guard #available(iOS 13, *) else {
            self.imageView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 130).isActive = true
            return
        }
        self.imageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        self.imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 76).isActive = true
    }
    
}

extension MenuView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuManager.shared.rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as? MenuTableViewCell {
            cell.setupCellInfo(MenuManager.shared.rows[indexPath.row], isSelected: MenuManager.shared.selectedIndex == indexPath.row)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        MenuManager.shared.selectedIndex = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class MenuTableViewCell: UITableViewCell {
    lazy var titleLabel: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.textAlignment = .center
        lb.textColor = Constants.MAIN_COLOR
        lb.text = "option"
        return lb
    }()
    
    override func didMoveToSuperview() {
        self.setupCell()
    }
    
    lazy var imageCell: UIImageView = {
        let obj = UIImageView()
        obj.contentMode = .scaleAspectFill
        obj.tintColor = Constants.MAIN_COLOR
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    private func setupCell() {
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.imageCell)
        
        self.imageCell.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 10).isActive = true
        self.imageCell.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        self.imageCell.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.imageCell.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
        self.titleLabel.leftAnchor.constraint(equalTo: self.imageCell.rightAnchor, constant: 10).isActive = true
        self.titleLabel.centerYAnchor.constraint(equalTo: self.imageCell.centerYAnchor).isActive = true
        
    }
    
    func setupCellInfo(_ data: MenuCell, isSelected: Bool) {
        self.titleLabel.text = data.labelTitle
        let color = isSelected ? Constants.MAIN_COLOR : .darkGray
        self.titleLabel.textColor = color
        self.imageCell.tintColor = color
        self.imageCell.image = data.image.withRenderingMode(.alwaysTemplate)
    }
}
