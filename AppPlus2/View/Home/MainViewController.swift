//
//  ViewController.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 03/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController {
    
    lazy var sideMenu: MenuView = {
        let menu = MenuView.shared
        menu.translatesAutoresizingMaskIntoConstraints = false
        menu.backgroundColor = Constants.MENU_BG_COLOR
        return menu
    }()
    
    lazy var tabBar: UITabBar = {
        let obj = UITabBar()
        
        return obj
    }()
    
    var userViewModel: UserViewModel?
    
    var selectedIndex = 0 {
        didSet{
            switch self.selectedIndex {
            case 0:
                
                break
            case 1:
                
                break
            case 2:
                
                break
            case 3:
                
                break
            case 4:
                
                break
            default:
                break
            }
        }
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.userViewModel?.delegate = self
        self.view.backgroundColor = Constants.VIEW_BG_COLOR
        self.setupViewController()
        self.tabBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupTabBar()
    }
    
    func setupTabBar() {
        
        let numberOfItems = CGFloat((self.tabBar.items?.count ?? 0))
        var safeArea: CGFloat = 0.0
        safeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        let tabBarItemSize = CGSize(width: (self.tabBar.frame.width) / numberOfItems, height: (self.tabBar.frame.height + safeArea))
        self.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: Constants.HOME_BAR_ITEM_COLOR ,size: tabBarItemSize).resizableImage(withCapInsets: .zero)
        self.tabBar.selectionIndicatorImage?.resizableImage(withCapInsets: UIEdgeInsets.init(top: 1, left: 0, bottom: 0, right: 0))
        self.tabBar.contentMode = .scaleAspectFill
        self.tabBar.frame.size.width = self.view.frame.width + numberOfItems
        self.tabBar.frame.origin.x = -2
        self.tabBar.tintColor = Constants.MAIN_COLOR
        self.tabBar.barTintColor = Constants.MAIN_COLOR
        self.tabBar.unselectedItemTintColor = Constants.HOME_BAR_ITEM_COLOR
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("tocou!!!")
        self.sideMenu.close()
        self.view.endEditing(true)
    }
    
    func setupViewController() {
        self.view.addSubview(self.tabBar)
        self.tabBar.items = Constants.BAR_HOME_ITEMS
        self.tabBar.selectedItem = self.tabBar.items?.first
        self.tabBar.translatesAutoresizingMaskIntoConstraints = false
        self.tabBar.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.tabBar.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tabBar.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tabBar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Navigation
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_menu"), style: .plain, target: self, action: #selector(self.menuHandle))
        self.navigationItem.titleView?.tintColor = Constants.NAVBAR_TEXT_COLOR
        self.navigationController?.navigationBar.barTintColor = Constants.NAVBAR_BG_COLOR
        self.navigationItem.title = Constants.NAV_TITLE
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Constants.NAVBAR_TEXT_COLOR]
        self.navigationItem.leftBarButtonItem?.tintColor = Constants.NAVBAR_TEXT_COLOR
        
        self.tabBar.layoutIfNeeded()
        
        self.view.addSubview(self.sideMenu)
        self.sideMenu.layoutMenuInSuperView(view: self.view)
        
    }
    
    @objc func menuHandle() {
        self.sideMenu.handleMenu()
    }
    
    deinit {
        print("fui...")
    }


}

extension MainViewController: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index = Constants.BAR_HOME_ITEMS.lastIndex(of: item) ?? 0
        self.selectedIndex = index
    }
}

extension MainViewController: UserViewModalDelegate {
    func didFinishedFetching(users: [User]) {
        print(users)
    }
}

