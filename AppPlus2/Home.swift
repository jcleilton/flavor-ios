//
//  Home.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 04/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import SwiftUI

@available(iOS 13.0.0, *)
struct Home: View {
    var body: some View {
        Text("Teste view!!")
    }
    
    func onDisappear(perform action: (() -> Void)? = nil) -> some View
    {
        print("Fui dessa vez na swiftui")
        return Text("Fui")
        
    }
    
    func onTapGesture(count: Int = 1, perform action: @escaping () -> Void) -> some View
    {
        return Text("Nao estou entendendo")
    }
    
}

@available(iOS 13.0.0, *)
struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}
