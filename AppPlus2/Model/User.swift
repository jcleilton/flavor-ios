//
//  User.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 06/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import Foundation
import CoreData


class User {
    
    var id: String?
    var name: String?
    var lastname: String?
    var email: String?
    var photo: String?
    
    private var userData: UserData?
    
    init(_ userData: UserData) {
        self.id = userData.id
        self.name = userData.name
        self.lastname = userData.lastname
        self.email = userData.email
        self.photo = userData.photo
        self.userData = userData
    }
    
    private init() {
        
    }
    
    convenience init(id: String?, name: String?, lastname: String?, email: String?, photo: String?) {
        self.init()
        self.id = id
        self.name = name
        self.lastname = lastname
        self.email = email
        self.photo = photo
        if let context = context {
            self.userData = UserData(context: context)
            do {
                try self.saveData()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    class func create(from dict: [String:Any]) -> User {
        let id = dict["codigo"] as? String ?? ""
        let name = dict["nome"] as? String ?? ""
        let email = dict["email"] as? String ?? ""
        let lastname = dict["sobrenome"] as? String ?? ""
        let photo = dict["image_url"] as? String ?? ""
        if let userData = getUser(by: id) {
            let user = User(userData)
            user.name = name
            user.lastname = lastname
            user.email = email
            user.photo = photo
            do {
                try user.saveData()
            } catch {
                print(error.localizedDescription)
            }
            return user
        } else {
            let user = User(id: id, name: name, lastname: lastname, email: email, photo: photo)
            return user
        }
    }
    
    public class func userFetchRequest() -> NSFetchRequest<UserData> {
        return NSFetchRequest<UserData>(entityName: "UserData")
    }
    
    func saveData() throws {
        if let context = context {
            if let userData = self.userData {
                userData.id = self.id
                userData.name = self.name
                userData.lastname = self.lastname
                userData.email = self.email
                userData.photo = self.photo
                try context.save()
            } else {
                throw DataFetchResultError.dataDosntExists
            }
        } else {
            throw DataFetchResultError.contextError
        }
    }
    
    func deleteData() throws {
        if let context = context {
            if let userData = self.userData {
                context.delete(userData)
            } else {
                throw DataFetchResultError.dataDosntExists
            }
        } else {
            throw DataFetchResultError.contextError
        }
    }
    
    class func getUser(by id: String) -> UserData? {
        let request = userFetchRequest()
        request.predicate = NSPredicate(format: "id = %@", id)
        request.returnsObjectsAsFaults = false
        if let context = context {
            do {
                let result = try context.fetch(request).first
                return result
            } catch {
                print(error.localizedDescription)
                return nil
            }
        } else {
            return nil
        }
    }
    
    class func listUsers() -> [User] {
        let request = userFetchRequest()
        request.returnsObjectsAsFaults = false
        if let context = context {
            do {
                let result = try context.fetch(request)
                return result.compactMap{User($0)}
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return []
    }
    
}
