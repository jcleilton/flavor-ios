//
//  Constants.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 04/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import Foundation
import UIKit

enum FLAVORS {
    case FLUX_DEV
    case FLUX_PROD
    case PETISCO_PROD
    case PETISCO_DEV
    case INTRAPLAST_PROD
    case INTRAPLAST_DEV
    case DEV
    case PROD
}

let user_loggedin = true

let flavor: FLAVORS = {
    var value: FLAVORS {
        #if FLUX_DEV
        return FLAVORS.FLUX_DEV
        #elseif FLUX_PROD
        return FLAVORS.FLUX_PROD
        #elseif PETISCO_PROD
        return FLAVORS.PETISCO_PROD
        #elseif PETISCO_DEV
        return FLAVORS.PETISCO_DEV
        #elseif INTRAPLAST_PROD
        return FLAVORS.INTRAPLAST_PROD
        #elseif INTRAPLAST_DEV
        return FLAVORS.INTRAPLAST_DEV
        #elseif DEV
        return FLAVORS.DEV
        #elseif PROD
        return FLAVORS.PROD
        #endif
    }
    
    return value
}()



open class Constants {
    
    
    static let MAIN_COLOR: UIColor = {
        switch flavor {
        case .DEV:
            return .black
        case .FLUX_DEV:
            return .white
        case .FLUX_PROD:
            return .black
        case .PETISCO_PROD:
            return .blue
        case .PETISCO_DEV:
            return .red
        case .INTRAPLAST_PROD:
            return .red
        case .INTRAPLAST_DEV:
            return .red
        case .PROD:
            return .brown
        }
    }()
    
    static let GOOGLESERVICE_PATH: String? = {
        switch flavor {
        case .PROD:
            print("[FIREBASE] Production mode has not a google configuration.")
            return nil
        case .FLUX_DEV:
            print("[FIREBASE] Flux DEV.")
            return Bundle.main.path(forResource: "GoogleService-Info3", ofType: "plist")
        case .FLUX_PROD:
            print("[FIREBASE] Flux PROD.")
            return Bundle.main.path(forResource: "GoogleService-Info3", ofType: "plist")
        case .PETISCO_PROD:
            print("[FIREBASE] Petisco DEV.")
            return Bundle.main.path(forResource: "GoogleService-Info2", ofType: "plist")
        case .PETISCO_DEV:
            print("[FIREBASE] Petisco DEV.")
            return Bundle.main.path(forResource: "GoogleService-Info2", ofType: "plist")
        case .INTRAPLAST_PROD:
            print("[FIREBASE] Intraplast PROD.")
            return Bundle.main.path(forResource: "GoogleService-Info1", ofType: "plist")
        case .INTRAPLAST_DEV:
            print("[FIREBASE] Intraplast DEV.")
            return Bundle.main.path(forResource: "GoogleService-Info1", ofType: "plist")
        case .DEV:
            print("[FIREBASE] DEV has not a google service.")
            return Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
        }
    }()
    
    static let VIEW_BG_COLOR: UIColor = {
        switch flavor {
        case .DEV:
            return .white
        case .FLUX_DEV:
            return .white
        case .FLUX_PROD:
            return .white
        case .PETISCO_PROD:
            return .white
        case .PETISCO_DEV:
            return .white
        case .INTRAPLAST_PROD:
            return .white
        case .INTRAPLAST_DEV:
            return .white
        case .PROD:
            return .white
        }
    }()
    
    static let NAVBAR_BG_COLOR: UIColor = {
        switch flavor {
        case .DEV:
            return .black
        case .FLUX_DEV:
            return .black
        case .FLUX_PROD:
            return .black
        case .PETISCO_PROD:
            return .blue
        case .PETISCO_DEV:
            return .blue
        case .INTRAPLAST_PROD:
            return .white
        case .INTRAPLAST_DEV:
            return .white
        case .PROD:
            return .brown
        }
    }()
    
    static let MENU_BG_COLOR: UIColor = {
        switch flavor {
        case .DEV:
            return .black
        case .FLUX_DEV:
            return .white
        case .FLUX_PROD:
            return UIColor.black.withAlphaComponent(0.8)
        case .PETISCO_PROD:
            return .blue
        case .PETISCO_DEV:
            return .blue
        case .INTRAPLAST_PROD:
            return .white
        case .INTRAPLAST_DEV:
            return .white
        case .PROD:
            return .brown
        }
    }()
    
    static let MENU_CELL: [MenuCell] = {
        switch flavor {
        case .DEV:
            return [
                MenuCell(labelTitle: "Home", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
                MenuCell(labelTitle: "Catálogo", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
                MenuCell(labelTitle: "Pedido", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
                MenuCell(labelTitle: "Títulos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
                MenuCell(labelTitle: "Favoritos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
                MenuCell(labelTitle: "Fale Conosco", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
                MenuCell(labelTitle: "Sobre o App", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            ]
        case .FLUX_DEV:
            return [
            MenuCell(labelTitle: "Home", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Catálogo", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Pedido", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Títulos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Favoritos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Fale Conosco", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Sobre o App", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            ]
        case .FLUX_PROD:
            return [
            MenuCell(labelTitle: "Home", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Catálogo", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Pedido", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Títulos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Favoritos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Fale Conosco", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Sobre o App", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            ]
        case .PETISCO_PROD:
            return [
            MenuCell(labelTitle: "Home", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Catálogo", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Pedido", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Títulos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Favoritos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Fale Conosco", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Sobre o App", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            ]
        case .PETISCO_DEV:
            return [
            MenuCell(labelTitle: "Home", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Catálogo", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Pedido", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Títulos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Favoritos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Fale Conosco", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Sobre o App", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            ]
        case .INTRAPLAST_PROD:
            return [
            MenuCell(labelTitle: "Home", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Catálogo", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Pedido", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Títulos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Favoritos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "IntraClub", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Fale Conosco", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Sobre o App", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            ]
        case .INTRAPLAST_DEV:
            return [
            MenuCell(labelTitle: "Home", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Catálogo", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Pedido", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Títulos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Favoritos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
             MenuCell(labelTitle: "IntraClub", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Fale Conosco", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Sobre o App", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            ]
        case .PROD:
            return [
            MenuCell(labelTitle: "Home", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Catálogo", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Pedido", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Títulos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Favoritos", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Fale Conosco", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            MenuCell(labelTitle: "Sobre o App", image: UIImage(named: "ic_home") ?? UIImage(), destiny: MainViewController()),
            ]
        }
    }()
    
    static let NAVBAR_TEXT_COLOR: UIColor = {
        switch flavor {
        case .DEV:
            return .white
        case .FLUX_DEV:
            return .yellow
        case .FLUX_PROD:
            return .yellow
        case .PETISCO_PROD:
            return .white
        case .PETISCO_DEV:
            return .white
        case .INTRAPLAST_PROD:
            return .red
        case .INTRAPLAST_DEV:
            return .red
        case .PROD:
            return .white
        }
    }()
    
    static let LOGO: UIImage = {
        switch flavor {
        case .DEV:
            return UIImage(named: "img_logo") ?? UIImage()
        case .FLUX_DEV:
            return UIImage(named: "img_logo_flux") ?? UIImage()
        case .FLUX_PROD:
            return UIImage(named: "img_logo_flux") ?? UIImage()
        case .PETISCO_PROD:
            return UIImage(named: "img_logo_petisco") ?? UIImage()
        case .PETISCO_DEV:
            return UIImage(named: "img_logo_petisco") ?? UIImage()
        case .INTRAPLAST_PROD:
            return UIImage(named: "img_logo_intraplast") ?? UIImage()
        case .INTRAPLAST_DEV:
            return UIImage(named: "img_logo_intraplast") ?? UIImage()
        case .PROD:
            return UIImage(named: "img_logo") ?? UIImage()
        }
    }()
    
    static let MENU_BTN_IMG: UIImage = {
        switch flavor {
        case .DEV:
            return (user_loggedin ?  UIImage(named: "ic_sair") ?? UIImage() : UIImage(named: "ic_sair") ?? UIImage()).withRenderingMode(.alwaysTemplate)
        case .FLUX_DEV:
            return (user_loggedin ?  UIImage(named: "ic_sair") ?? UIImage() : UIImage(named: "ic_sair") ?? UIImage()).withRenderingMode(.alwaysTemplate)
        case .FLUX_PROD:
            return (user_loggedin ?  UIImage(named: "ic_sair") ?? UIImage() : UIImage(named: "ic_sair") ?? UIImage()).withRenderingMode(.alwaysTemplate)
        case .PETISCO_PROD:
            return (user_loggedin ?  UIImage(named: "ic_sair") ?? UIImage() : UIImage(named: "ic_sair") ?? UIImage()).withRenderingMode(.alwaysTemplate)
        case .PETISCO_DEV:
            return (user_loggedin ?  UIImage(named: "ic_sair") ?? UIImage() : UIImage(named: "ic_sair") ?? UIImage()).withRenderingMode(.alwaysTemplate)
        case .INTRAPLAST_PROD:
            return (user_loggedin ?  UIImage(named: "ic_sair") ?? UIImage() : UIImage(named: "ic_sair") ?? UIImage()).withRenderingMode(.alwaysTemplate)
        case .INTRAPLAST_DEV:
            return (user_loggedin ?  UIImage(named: "ic_sair") ?? UIImage() : UIImage(named: "ic_sair") ?? UIImage()).withRenderingMode(.alwaysTemplate)
        case .PROD:
            return (user_loggedin ?  UIImage(named: "ic_sair") ?? UIImage() : UIImage(named: "ic_sair") ?? UIImage()).withRenderingMode(.alwaysTemplate)
        }
    }()
    
    static let MENU_BTN_TXT: String = {
        switch flavor {
        case .DEV:
            return user_loggedin ? "Sair" : "Entrar"
        case .FLUX_DEV:
            return user_loggedin ? "Sair" : "Entrar"
        case .FLUX_PROD:
            return user_loggedin ? "Sair" : "Entrar"
        case .PETISCO_PROD:
            return user_loggedin ? "Sair" : "Entrar"
        case .PETISCO_DEV:
            return user_loggedin ? "Sair" : "Entrar"
        case .INTRAPLAST_PROD:
            return user_loggedin ? "Sair" : "Entrar"
        case .INTRAPLAST_DEV:
            return user_loggedin ? "Sair" : "Entrar"
        case .PROD:
            return user_loggedin ? "Sair" : "Entrar"
        }
    }()
    
    static let SECONDARY_COLOR: UIColor = {
        switch flavor {
        case .DEV:
            return .white
        case .FLUX_DEV:
            return .yellow
        case .FLUX_PROD:
            return .yellow
        case .PETISCO_PROD:
            return .white
        case .PETISCO_DEV:
            return .white
        case .INTRAPLAST_PROD:
            return .yellow
        case .INTRAPLAST_DEV:
            return .yellow
        case .PROD:
            return .cyan
        }
    }()
    
    static let HOME_BAR_ITEM_COLOR: UIColor = {
        switch flavor {
        case .DEV:
            return .white
        case .FLUX_DEV:
            return .white
        case .FLUX_PROD:
            return .white
        case .PETISCO_PROD:
            return .white
        case .PETISCO_DEV:
            return .white
        case .INTRAPLAST_PROD:
            return .white
        case .INTRAPLAST_DEV:
            return .white
        case .PROD:
            return .white
        }
    }()

    
    static let NAV_TITLE: String = {
        switch flavor {
        case .DEV:
            return "AppPlus Dev"
        case .FLUX_DEV:
            return "Flux Dev"
        case .FLUX_PROD:
            return "Flux"
        case .PETISCO_PROD:
            return "Petisco"
        case .PETISCO_DEV:
            return "Petisco Dev"
        case .INTRAPLAST_PROD:
            return "Intraplast"
        case .INTRAPLAST_DEV:
            return "Intraplast Dev"
        case .PROD:
            return "AppPlus"
        }
    }()
    
    static let APP_NAME: String = {
        switch flavor {
        case .DEV:
            return "AppPlus Dev"
        case .FLUX_DEV:
            return "Flux Dev"
        case .FLUX_PROD:
            return "Flux"
        case .PETISCO_PROD:
            return "Petisco"
        case .PETISCO_DEV:
            return "Petisco Dev"
        case .INTRAPLAST_PROD:
            return "Intraplast"
        case .INTRAPLAST_DEV:
            return "Intraplast Dev"
        case .PROD:
            return "AppPlus"
        }
    }()
    
    static let BAR_HOME_ITEMS: [UITabBarItem] = {
        
        //recentes
        let recentesItem = UITabBarItem(title: "Recentes", image: UIImage(named: "ic_recentes"), selectedImage: UIImage(named: "ic_recentes_selected"))
        //promocoes
        let promocoesItem = UITabBarItem(title: "Recentes", image: UIImage(named: "ic_recentes"), selectedImage: UIImage(named: "ic_recentes_selected"))
        //receitas
        let receitasItem = UITabBarItem(title: "Recentes", image: UIImage(named: "ic_recentes"), selectedImage: UIImage(named: "ic_recentes_selected"))
        //dicas
        let dicasItem = UITabBarItem(title: "Recentes", image: UIImage(named: "ic_recentes"), selectedImage: UIImage(named: "ic_recentes_selected"))
        
        switch flavor {
        case .DEV:
            return []
        case .FLUX_DEV:
            return []
        case .FLUX_PROD:
            return []
        case .PETISCO_PROD:
            return []
        case .PETISCO_DEV:
            return []
        case .INTRAPLAST_PROD:
            return [recentesItem, promocoesItem, receitasItem, dicasItem]
        case .INTRAPLAST_DEV:
            return []
        case .PROD:
            return []
        }
    }()
    
    static let ROOT_URL: String = {
        switch flavor {
        case .DEV:
            return ""
        case .FLUX_DEV:
            return ""
        case .FLUX_PROD:
            return ""
        case .PETISCO_PROD:
            return ""
        case .PETISCO_DEV:
            return ""
        case .INTRAPLAST_PROD:
            return ""
        case .INTRAPLAST_DEV:
            return ""
        case .PROD:
            return ""
        }
    }()
    
    static let EMPRESA_ID: String = {
        switch flavor {
        case .DEV:
            return ""
        case .FLUX_DEV:
            return ""
        case .FLUX_PROD:
            return ""
        case .PETISCO_PROD:
            return ""
        case .PETISCO_DEV:
            return ""
        case .INTRAPLAST_PROD:
            return ""
        case .INTRAPLAST_DEV:
            return ""
        case .PROD:
            return ""
        }
    }()

}
