//
//  Global.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 05/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import Foundation
import UIKit
import CoreData

var context: NSManagedObjectContext? = {
    if let app = UIApplication.shared.delegate as? AppDelegate {
        return app.persistentContainer.viewContext
    } else {
        return nil
    }
}()
