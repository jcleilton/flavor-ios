//
//  ConnectionManager.swift
//  AppPlus2
//
//  Created by KARYNE DE ARAUJO on 06/12/19.
//  Copyright © 2019 KARYNE DE ARAUJO. All rights reserved.
//

import UIKit


struct ConnectionSessionManager {
    typealias Callback<T: Any, E: Any> = (T, E) -> Void
    static var session = URLSession.shared
    
    static func invoke(url: String, withArgs args: Dictionary<String, Any>, httpMethod: ConnectionSessionHttpMethod, completion: Callback<Any?, NSError?>?){
        URLCache.shared.removeAllCachedResponses()
        guard let url = URL(string: url) else{
            completion?(nil, ConnectionSessionError.invalidUrl as NSError?)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.description()
        
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: args, options: .prettyPrinted)
        }catch{
            
            completion?(nil, error as NSError?)
        }
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let error = error else{
                guard let data = data else {
                    completion?(nil, NSError())
                    return
                }
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                    completion?(json, nil)
                    return
                }catch{
                    completion?(nil, error as NSError?)
                    return
                }
            }
            completion?(nil, error as NSError?)
        }).resume()
    }
    
    static func invokeEncrypted(url: String, withArgs args: Dictionary<String, Any>, httpMethod: ConnectionSessionHttpMethod, completion: Callback<Any?, ConnectionSessionError?>?){
        URLCache.shared.removeAllCachedResponses()
        guard let url = URL(string: url) else{
            completion?(nil, ConnectionSessionError.invalidUrl)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.description()
        request.addValue("Accept", forHTTPHeaderField: "application/json")
        request.addValue("Content-type", forHTTPHeaderField: "application/json")
        
        do{
            let json = try JSONSerialization.data(withJSONObject: args, options: .prettyPrinted)
            let hexStr = json.hexEncodedString()
            let base64Str = hexStr.base64Encoded()
            request.httpBody = base64Str?.data(using: .utf8)
        }catch{
            completion?(nil, ConnectionSessionError.invalidJSON)
        }
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let _ = error else{
                guard let data = data else {
                    completion?(nil, ConnectionSessionError.invalidJSON)
                    return
                }
                do{
                    let string64data = String(data: data, encoding: .utf8)?.replacingOccurrences(of: "\n", with: "", options: .literal, range: nil)
                    let strigData = string64data?.base64Decoded()
                    guard let jsonData = strigData?.hexa2Data else{
                        completion?(nil, ConnectionSessionError.notFound)
                        return
                    }
                    let json = try JSONSerialization.jsonObject(with: jsonData , options: .mutableContainers)
                    completion?(json, nil)
                    return
                }catch{
                    completion?(nil, ConnectionSessionError.invalidJSON)
                    return
                }
            }
            completion?(nil, ConnectionSessionError.notFound)
        }).resume()
    }
    
    static func invokeEncrypted(url: String, withArgs args: Dictionary<String, Any>, httpMethod: ConnectionSessionHttpMethod, compression: Compression, completion: Callback<Any?, ConnectionSessionError?>?){
        URLCache.shared.removeAllCachedResponses()
        guard let url = URL(string: url) else{
            completion?(nil, ConnectionSessionError.invalidUrl)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.description()
        request.addValue("Accept", forHTTPHeaderField: "application/json")
        request.addValue("Content-type", forHTTPHeaderField: "application/json")
        
        do{
            let json = try JSONSerialization.data(withJSONObject: args, options: .prettyPrinted)
            let hexStr = json.hexEncodedString()
            let base64Str = hexStr.base64Encoded()
            request.httpBody = base64Str?.data(using: .utf8)?.compressed(using: compression)
        }catch{
            completion?(nil, ConnectionSessionError.invalidJSON)
        }
        
        
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let _ = error else{
                do{
                    guard let dataUnzipped = data?.uncompressed(using: compression) else{
                        completion?(nil, ConnectionSessionError.unzipFailed)
                        return
                    }
                    let string64data = String(data: dataUnzipped, encoding: .utf8)?.replacingOccurrences(of: "\n", with: "", options: .literal, range: nil)
                    let strigData = string64data?.base64Decoded()
                    guard let jsonData = strigData?.hexa2Data else{
                        completion?(nil, ConnectionSessionError.notFound)
                        return
                    }
                    let json = try JSONSerialization.jsonObject(with: jsonData , options: .mutableContainers)
                    completion?(json, nil)
                    return
                }catch{
                    completion?(nil, ConnectionSessionError.invalidJSON)
                    return
                }
            }
            completion?(nil, ConnectionSessionError.notFound)
        }).resume()
    }
    
    static func downloadImage(from url: URL, completion: @escaping Callback<UIImage?, Error?>){
        URLCache.shared.removeAllCachedResponses()
        session.dataTask(with: url, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                guard let error = error else{
                    guard let data = data else{
                        return completion(nil, nil)
                    }
                    let image = UIImage(data: data)
                    return completion(image, nil)
                }
                completion(nil, error)
            }
        }).resume()
    }
    
    static func sendEmail(idCompany: Int, from: String, to: String, cc: String, name: String, phone: String, message: String, subject: String, completion: @escaping Callback<Bool, Error?>){
        ConnectionSessionManager.invoke(url: "\(Constants.ROOT_URL)/email", withArgs: ["to": to, "from": from, "cc": cc, "title": subject, "name": name, "fone": phone, "msg":message, "empresa_id": idCompany], httpMethod: .post, completion: {(response, error) in
            guard let jsonResponse = response as? Dictionary<String, Any> else{
                return completion(false, nil)
            }
            DispatchQueue.main.async {
                let success = jsonResponse["status"] as? Bool ?? false
                completion(success, nil)
            }
        })
    }
    
    static func isNeedUpdate(completion: @escaping (_ error: Error?, _ response: Bool)->Void){
        ConnectionSessionManager.invoke(url: "\(Constants.ROOT_URL)/appversion", withArgs: ["empresa_id": Constants.EMPRESA_ID, "os":"ios", "versao": Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""], httpMethod: .post, completion: {(response, error) in
            guard let jsonResponse = response as? Dictionary<String, Any> else{
                return completion(NSError() as Error, false)
            }
            DispatchQueue.main.async {
                let success = jsonResponse["status"] as? Bool ?? false
                completion(nil, success)
            }
        })
    }
    
    static func send(imageData: Data, completion: @escaping (_ error: Error?)->Void){
        let imageBase64 = "data:image/jpeg:Base64, \(imageData.base64EncodedString())"
        ConnectionSessionManager.invokeEncrypted(url: "\(Constants.ROOT_URL)/imagem", withArgs: ["image64": imageBase64], httpMethod: .post) { (result, error) in
            print(result ?? "result send.")
            completion(error)
        }
    }
    
    static func validateCpfCnpj (idCompany: Int, cpfCnpj: String, completion: @escaping Callback<Bool, String>) {
        ConnectionSessionManager.invoke(url: "\(Constants.ROOT_URL)/validarcpfcnpj", withArgs: ["empresa_id":idCompany, "cpf_cnpj":cpfCnpj], httpMethod: .post, completion: {(response, error) in
            guard let jsonResponse = response as? Dictionary<String, Any> else{
                return completion(false, "")
            }
            let menssage = jsonResponse["info"] as? String ?? ""
            DispatchQueue.main.async {
                let success = jsonResponse["status"] as? Bool ?? false
                completion(success, menssage)
            }
        })
    }
}
